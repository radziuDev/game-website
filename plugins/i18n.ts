import { createI18n } from 'vue-i18n'
import pl from '../locales/pl.json'

export default defineNuxtPlugin(({ vueApp }) => {
  const i18n = createI18n({
    legacy: false,
    globalInjection: true,
    locale: 'pl',
    messages: { pl },
  })

  vueApp.use(i18n)
})
