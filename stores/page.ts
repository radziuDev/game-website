interface State {
  acctualPage: number;
  maxPages: number;
};

export const usePageStore = defineStore({
  id: 'PageStore',
  state: (): State => {
    return {
      acctualPage: 0,
      maxPages: 0,
    };
  },

  getters: {
    getPage(state) {
      return state.acctualPage;
    },

    getMaxPages(state) {
      return state.maxPages;
    },
  },

  actions: {
    async changePage(pageNumber: number) {
      this.acctualPage = pageNumber;
    },

    async setMaxPages(maxPageNumber: number) {
      this.maxPages = maxPageNumber;
    },
  },
});

